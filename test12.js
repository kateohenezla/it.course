onload = () => {
    const canvas = document.createElement('canvas');
    document.body.appendChild(canvas);

    canvas.width = 750;
    canvas.height = 600;

    let size = 15

    const ctx = canvas.getContext('2d');

    ctx.fillStyle = `rgb(40,46,0)`;
    ctx.fillRect(0, 0, canvas.width, canvas.height);


    let brush = document.getElementById('BRUSH1');
    const brushCanvas = document.createElement('canvas')
    brushCanvas.style = `display:none`;
    brushCanvas.width = brush.width;
    brushCanvas.height = brush.height;
    const brushCtx = brushCanvas.getContext('2d');


    const selectColor = (r, g, b) => {
        brushCtx.clearRect(0, 0, brushCanvas.width, brushCanvas.height);
        brushCtx.drawImage(brush, 0, 0, brushCanvas.width, brushCanvas.height);
        const imageData = brushCtx.getImageData(0, 0, brushCanvas.width, brushCanvas.height);
        const pixels = imageData.data;

        for (let y = 0; y < imageData.height; y++) {
            for (let x = 0; x < imageData.width; x++) {
                const offset = 4 * (y * imageData.width + x);
                pixels[offset + 0] = Math.floor(r * pixels[offset + 0] / 255);
                pixels[offset + 1] = Math.floor(g * pixels[offset + 1] / 255);
                pixels[offset + 2] = Math.floor(b * pixels[offset + 2] / 255);
            }
        }
        brushCtx.putImageData(imageData, 0, 0);
    };

    selectColor(255, 255, 255);

    const drawBrush = (x, y) => {
        ctx.drawImage(brushCanvas, x - size / 2, y - size / 2, size, size)

    };


    const getMouseCoords = e => {
        const rect = canvas.getBoundingClientRect();
        return [e.clientX - rect.x, e.clientY - rect.y];
    };


    let prevX = null;
    let prevY = null;


    canvas.onmousedown = e => {
        if (e.button !== 0) { return; }

        [prevX, prevY] = getMouseCoords(e);

        drawBrush(prevX, prevY, size);
    };

    onmousemove = e => {
        if (prevX === null) { return; }

        [x, y] = getMouseCoords(e);
        forEachPixel(prevX, prevY, x, y, drawBrush);
        prevX = x;
        prevY = y;
    };

    onmouseup = e => {
        if (e.button !== 0) { return; }

        prevX = null;
        prevY = null;
    };

    const Button0 = document.getElementById('Button0');

    function change() {
        //ctx.fillStyle= `rgb(40,46,0)`;
        ctx.fillRect(0, 0, canvas.width, canvas.height);
    }
    Button0.addEventListener('click', change);

    const Button1 = document.getElementById('Button1');
    const Button2 = document.getElementById('Button2');
    const Button3 = document.getElementById('Button3');
    const Button4 = document.getElementById('Button4');
    const Button5 = document.getElementById('Button5');


    const Button6 = document.getElementById('Button6');
    const Button7 = document.getElementById('Button7');

    const Button9 = document.getElementById('Button9');
    const Button10 = document.getElementById('Button10');
    const Button11 = document.getElementById('Button11');
    const Button12 = document.getElementById('Button12');
    const Button13 = document.getElementById('Button13');


    function brush_first() {
        brush = document.getElementById('BRUSH1');
    }

    function brush_second() {
        brush = document.getElementById('BRUSH2');
    }

    function brush_third() {
        brush = document.getElementById('BRUSH3');
    }

    function brush_fourth() {
        brush = document.getElementById('BRUSH4');
    }

    function brush_fifth() {
        brush = document.getElementById('BRUSH5');
    }

    function plus() {
        size += 5;
    }

    function minus() {
        size -= 5;
    }

    function white() {
        selectColor(255, 255, 255)
    }

    function black() {
        selectColor(0, 0, 0)

    }
    function red() {
        selectColor(255, 0, 0)
    }

    function green() {
        selectColor(0, 255, 0)
    }

    function blue() {
        selectColor(0, 0, 255)
    }


    Button9.addEventListener('click', white);
    Button10.addEventListener('click', black);
    Button11.addEventListener('click', red);
    Button12.addEventListener('click', green);
    Button13.addEventListener('click', blue);




    Button1.addEventListener('click', brush_first);
    Button2.addEventListener('click', brush_second);
    Button3.addEventListener('click', brush_third);
    Button4.addEventListener('click', brush_fourth);
    Button5.addEventListener('click', brush_fifth);



    Button6.addEventListener('click', plus);
    Button7.addEventListener('click', minus);


};


function forEachPixel(x0, y0, x1, y1, action) {
    x0 = Math.round(x0)
    x1 = Math.round(x1)
    y0 = Math.round(y0)
    y1 = Math.round(y1)

    const dx = x1 - x0;
    const dy = y1 - y0;

    action(x0, y0);

    if (dx == 0 && dy == 0) { return; }

    const maxDiff = Math.max(Math.abs(dx), Math.abs(dy));
    let x = x0;
    let y = y0;
    for (let i = 0; i < maxDiff; i++) {
        x += dx / maxDiff;
        y += dy / maxDiff;

        action(x, y);
    }
}

